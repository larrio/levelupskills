function LVLUP_LevelUpWarriorArms(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_WARRIOR_ARMS_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpWarriorFury(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_WARRIOR_FURY_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpWarriorProtection(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_WARRIOR_PROTECTION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end