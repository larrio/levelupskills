local LVLUP_LevelUpFrame = CreateFrame("Frame")

local LVLUP_EventsLevelUp = {}

--function LVLUP_EventsLevelUp:PLAYER_LEVEL_UP(...)
function LVLUP_EventsLevelUp:PLAYER_LEVEL_UP(...)
	local new_level, _, _, _, _, _, _, _, _ = ...
	Screenshot()
	print("Smile for the camera ! A screenshot was taken")
	
	if (LVLUP_PlayerClass == "WARRIOR") then
		if (LVLUP_PlayerSpec == "ARMS") then
			LVLUP_LevelUpWarriorArms(new_level)
		elseif (LVLUP_PlayerSpec == "FURY") then
			LVLUP_LevelUpWarriorFury(new_level)
		elseif (LVLUP_PlayerSpec == "PROTECTION") then
			LVLUP_LevelUpWarriorProtection(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "PALADIN") then
		if (LVLUP_PlayerSpec == "RETRIBUTION") then
			LVLUP_LevelUpPaladinRetribution(new_level)
		elseif (LVLUP_PlayerSpec == "PROTECTION") then
			LVLUP_LevelUpPaladinProtection(new_level)
		elseif (LVLUP_PlayerSpec == "HOLY") then 
			LVLUP_LevelUpPaladinHoly(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "HUNTER") then
		if (LVLUP_PlayerSpec == "SURVIVAL") then
			LVLUP_LevelUpHunterSurvival(new_level)
		elseif (LVLUP_PlayerSpec == "BEASTMASTER") then
			LVLUP_LevelUpHunterBeastMaster(new_level)
		elseif (LVLUP_PlayerSpec == "MARKSMANSHIP") then 
			LVLUP_LevelUpHunterMarksmanship(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "ROGUE") then
		if (LVLUP_PlayerSpec == "COMBAT") then
			LVLUP_LevelUpRogueCombat(new_level)
		elseif (LVLUP_PlayerSpec == "ASSASSINATION") then
			LVLUP_LevelUpRogueAssassination(new_level)
		elseif (LVLUP_PlayerSpec == "SUBTETLY") then 
			LVLUP_LevelUpRogueSubtetly(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "PRIEST") then
		if (LVLUP_PlayerSpec == "SHADOW") then
			LVLUP_LevelUpPriestShadow(new_level)
		elseif (LVLUP_PlayerSpec == "DISCIPLINE") then
			LVLUP_LevelUpPriestDiscipline(new_level)
		elseif (LVLUP_PlayerSpec == "HOLY") then 
			LVLUP_LevelUpPriestHoly(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "SHAMAN") then
		if (LVLUP_PlayerSpec == "ENHANCEMENT") then
			LVLUP_LevelUpShamanEnhancement(new_level)
		elseif (LVLUP_PlayerSpec == "RESTORATION") then
			LVLUP_LevelUpShamanRestoration(new_level)
		elseif (LVLUP_PlayerSpec == "ELEMENTAL") then 
			LVLUP_LevelUpShamanElemental(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "MAGE") then
		if (LVLUP_PlayerSpec == "FROST") then
			LVLUP_LevelUpMageFrost(new_level)
		elseif (LVLUP_PlayerSpec == "FIRE") then
			LVLUP_LevelUpMageFire(new_level)
		elseif (LVLUP_PlayerSpec == "ARCANE") then 
			LVLUP_LevelUpMageArcane(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "WARLOCK") then
		if (LVLUP_PlayerSpec == "DEMONOLOGY") then
			LVLUP_LevelUpWarlockDemonology(new_level)
		elseif (LVLUP_PlayerSpec == "AFFLICTION") then
			LVLUP_LevelUpWarlockAffliction(new_level)
		elseif (LVLUP_PlayerSpec == "DESTRUCTION") then 
			LVLUP_LevelUpWarlockDestruction(new_level)
		else
			-- TODO : no spec choosen
		end
	elseif (LVLUP_PlayerClass == "DRUID") then
		if (LVLUP_PlayerSpec == "FERAL") then
			LVLUP_LevelUpDruidFeral(new_level)
		elseif (LVLUP_PlayerSpec == "BALANCE") then
			LVLUP_LevelUpDruidBalance(new_level)
		elseif (LVLUP_PlayerSpec == "RESTORATION") then 
			LVLUP_LevelUpDruidRestoration(new_level)
		else
			-- TODO : no spec choosen
		end
	else
		-- TODO : no class found
	end	
end

function LVLUP_ProcessLevelUp(LVLUP_CLASS_ID_SPELL_INFO, new_level)
	local should_learn = false
	local number_of_useful_spell_to_learn = 0
	print("Congratulations for your level up ! The following spells can be learn at your master :")
	for level, spells in pairs (LVLUP_CLASS_ID_SPELL_INFO[new_level]) do
		local spell_name, spell_rank, spell_icon, _, _, _, _ = GetSpellInfo(spells[1])
		if (spells[2] == true) then
			should_learn = true
			number_of_useful_spell_to_learn = number_of_useful_spell_to_learn + 1
			print(spell_name .. " (Rank " .. spell_rank .. " --> (You should learn this one).")
		else
			print(spell_name .. " --> (Not necessary, it's better to save your money).")
		end
	end
	
	if (should_learn == true) then
		print("In conclusion : You should go back to your master on this level, you have " .. number_of_useful_spell_to_learn .. " useful spells to learn this level !")
	else
		print("In conclusion : You don't need to go back to your master on this level.")
	end
end

function LVLUP_CongratsMaxLevel()
	print("CONGRATULATIONS !!!!!!!")
	print("Go learn all your other spells now ! You deserve it ! Well done !")
end

LVLUP_LevelUpFrame:SetScript("OnEvent", function(self, event, ...)
	LVLUP_EventsLevelUp[event](self, ...)
end)

for event, _ in pairs (LVLUP_EventsLevelUp) do
	LVLUP_LevelUpFrame:RegisterEvent(event)
end