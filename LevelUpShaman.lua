function LVLUP_LevelUpShamanEnhancement(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			print("Don't forget to do your fire totem quest as soon as possible")
		elseif (new_level == 20) then
			print("Don't forget to do your water totem quest as soon as possible.")
		elseif (new_level == 30) then
			print("Don't forget to do your air totem quest as soon as possible")
		elseif (new_level == 40) then
			print("Congratulations for your mount !")
		end
	
		LVLUP_ProcessLevelUp(LVLUP_SHAMAN_ENHANCEMENT_ID_SPELL_INFO, new_level)
	else
		print("CONGRATULATIONS !!!!!!!")
		print("Go learn all your other spells now ! You deserve it ! Well done !")
	end
end

function LVLUP_LevelUpShamanElemental(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		LVLUP_ProcessLevelUp(LVLUP_SHAMAN_ELEMENTAL_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpShamanRestoration(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		LVLUP_ProcessLevelUp(LVLUP_SHAMAN_RESTORATION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end