LVLUP_PlayerName = "NONE"
LVLUP_PlayerClass = "NONE"
LVLUP_PlayerRealm = "NONE"
LVLUP_PlayerSpec = "NONE"


local function getPlayerName()
	LVLUP_PlayerName = UnitName("player")
end

local function getPlayerSpec()
	

	if (LVLUP_PlayerClass == "WARRIOR") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "PALADIN") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "HUNTER") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "ROGUE") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "PRIEST") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "SHAMAN") then
		-- TODO : suggest 3 accurate spec
		LVLUP_PlayerSpec = "ENHANCEMENT"
	elseif (LVLUP_PlayerClass == "MAGE") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "WARLOCK") then
		-- TODO : suggest 3 accurate spec
	elseif (LVLUP_PlayerClass == "DRUID") then
		-- TODO : suggest 3 accurate spec
	else
		-- TODO : no class found
	end	
	
end

local function getPlayerClass()
	local localizedClass, englishClass, classIndex = UnitClass("player");	--[[ None = 0 Warrior = 1 Paladin = 2 Hunter = 3 Rogue = 4 Priest = 5 Shaman = 7 Mage = 8 Warlock = 9 Druid = 11 ]]
	
	if (classIndex == 1) then
		LVLUP_PlayerClass = "WARRIOR"
	elseif (classIndex == 2) then
		LVLUP_PlayerClass = "PALADIN"
	elseif (classIndex == 3) then
		LVLUP_PlayerClass = "HUNTER"
	elseif (classIndex == 4) then
		LVLUP_PlayerClass = "ROGUE"
	elseif (classIndex == 5) then
		LVLUP_PlayerClass = "PRIEST"
	elseif (classIndex == 7) then
		LVLUP_PlayerClass = "SHAMAN"
	elseif (classIndex == 8) then
		LVLUP_PlayerClass = "MAGE"
	elseif (classIndex == 9) then
		LVLUP_PlayerClass = "WARLOCK"
	else
		LVLUP_PlayerClass = "DRUID"
	end
end

local function getPlayerRealm()
	LVLUP_PlayerRealm = GetRealmName()
end

local LVLUP_InitFrame = CreateFrame("Frame")

local LVLUP_InitEvents = {}

function LVLUP_InitEvents:PLAYER_ENTERING_WORLD(...)
	getPlayerName()
	getPlayerClass()
	getPlayerRealm()
	getPlayerSpec()
end

LVLUP_InitFrame:SetScript("OnEvent", function(self, event, ...)
	LVLUP_InitEvents[event](self, ...)
end)

for event, _ in pairs (LVLUP_InitEvents) do
	LVLUP_InitFrame:RegisterEvent(event)
end
