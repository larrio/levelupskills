function LVLUP_LevelUpRogueCombat(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_ROGUE_COMBAT_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpRogueAssassination(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_ROGUE_ASSASSINATION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpRogueSubtetly(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_ROGUE_SUBTETLY_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end