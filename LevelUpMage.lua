function LVLUP_LevelUpMageFrost(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_MAGE_FROST_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpMageFire(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_MAGE_FIRE_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpMageArcane(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_MAGE_ARCANE_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end