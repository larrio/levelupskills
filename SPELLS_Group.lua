LVLUP_SHAMAN_ENHANCEMENT_ID_SPELL_INFO = {
	[2] = {{"8071", "false"}, {"8042", "true"},},
	[6] = {{"332", "false"}, {"2484", "false"},},
	[8] = {{"324", "true"}, {"8018", "true"}, {"5730", "false"}, {"8044", "true"}, {"529", "true"},},
	[10] = {{"8024", "false"}, {"8075", "true"}, {"8050", "false"}, {"3599", "false"},},
	[12] = {{"547", "false"}, {"2008", "true"}, {"370", "true"}, {"1535", "false"},},
	[14] = {{"8154", "false"}, {"8045", "true"}, {"548", "true"},},
	[16] = {{"325", "true"}, {"8019", "true"},},
	[18] = {{"8027", "false"}, {"913", "false"}, {"8143", "false"}, {"6390", "false"}, {"8052", "false"},},
	[20] = {{"2645", "true"}, {"8033", "false"}, {"8004", "true"}, {"5394", "false"}, {"8056", "false"}, {"915", "true"}, {"6363", "false"},},
	[22] = {{"131", "false"}, {"8498", "false"},},
	[24] = {{"905", "true"}, {"10399", "true"}, {"8181", "false"}, {"8155", "false"}, {"8160", "true"}, {"939", "true"}, {"20609", "false"}, {"8046", "true"},},
	[26] = {{"6196", "false"}, {"8030", "false"}, {"5675", "false"}, {"943", "false"}, {"8190", "false"},},
	[28] = {{"8227", "false"}, {"8038", "false"}, {"8184", "false"}, {"546", "false"}, {"8008", "false"}, {"6391", "false"}, {"8053", "false"},},
	[30] = {{"8232", "true"}, {"8177", "false"}, {"10595", "false"}, {"556", "true"}, {"6375", "false"}, {"20608", "true"}, {"6364", "false"},},
	[32] = {{"945", "true"}, {"8512", "true"}, {"959", "true"}, {"8012", "true"}, {"6041", "false"}, {"421", "true"}, {"8499", "false"},},
	[34] = {{"6495", "false"}, {"10406", "false"}, {"8058", "false"},},
	[36] = {{"16339", "false"}, {"8010", "false"}, {"10495", "true"}, {"20610", "false"}, {"10412", "true"}, {"10585", "false"},},
	[38] = {{"10478", "false"}, {"8249", "false"}, {"8161", "true"}, {"10456", "false"}, {"8170", "false"}, {"6392", "false"}, {"10391", "false"},},
	[40] = {{"8134", "true"}, {"8235", "true"}, {"8005", "true"}, {"6377", "false"}, {"1064", "true"}, {"10447", "false"}, {"930", "false"}, {"6365", "false"}},
	[42] = {{"10537", "false"}, {"11314", "false"},},
	[44] = {{"10600", "false"}, {"10407", "false"}, {"10466", "false"}, {"10392", "false"},},
	[46] = {{"16341", "false"}, {"10496", "false"}, {"10622", "false"}, {"10472", "false"}, {"10586", "false"}, },
	[48] = {{"10431", "true"}, {"16355", "false"}, {"10526", "false"}, {"10395", "true"}, {"20776", "false"}, {"10413", "true"}, {"10427", "false"}, {"2860", "false"},},
	[50] = {{"10486", "true"}, {"10462", "false"}, {"15207", "false"}, {"10437", "false"},},
	[52] = {{"10442", "false"}, {"10467", "false"}, {"10448", "false"}, {"11315", "false"},},
	[54] = {{"10479", "false"}, {"10408", "false"}, {"10623", "false"}, },
	[56] = {{"10432", "true"}, {"16342", "false"}, {"10396", "true"}, {"10497", "false"}, {"15208", "false"}, {"10605", "false"}, {"10587", "false"},},
	[58] = {{"10538", "false"}, {"16356", "false"}, {"16387", "false"}, {"10428", "false"}, {"10473", "false"},},
}

LVLUP_SHAMAN_ELEMENTAL_ID_SPELL_INFO = { -- TODO
	[2] = {{"8071", "false"}, {"8042", "true"},},
	[6] = {{"332", "false"}, {"2484", "false"},},
	[8] = {{"324", "true"}, {"8018", "true"}, {"5730", "false"}, {"8044", "true"}, {"529", "true"},},
	[10] = {{"8024", "false"}, {"8075", "true"}, {"8050", "false"}, {"3599", "false"},},
	[12] = {{"547", "false"}, {"2008", "true"}, {"370", "true"}, {"1535", "false"},},
	[14] = {{"8154", "false"}, {"8045", "true"}, {"548", "true"},},
	[16] = {{"325", "true"}, {"8019", "true"},},
	[18] = {{"8027", "false"}, {"913", "false"}, {"8143", "false"}, {"6390", "false"}, {"8052", "false"},},
	[20] = {{"2645", "true"}, {"8033", "false"}, {"8004", "true"}, {"5394", "false"}, {"8056", "false"}, {"915", "true"}, {"6363", "false"},},
	[22] = {{"131", "false"}, {"8498", "false"},},
	[24] = {{"905", "true"}, {"10399", "true"}, {"8181", "false"}, {"8155", "false"}, {"8160", "true"}, {"939", "true"}, {"20609", "false"}, {"8046", "true"},},
	[26] = {{"6196", "false"}, {"8030", "false"}, {"5675", "false"}, {"943", "false"}, {"8190", "false"},},
	[28] = {{"8227", "false"}, {"8038", "false"}, {"8184", "false"}, {"546", "false"}, {"8008", "false"}, {"6391", "false"}, {"8053", "false"},},
	[30] = {{"8232", "true"}, {"8177", "false"}, {"10595", "false"}, {"556", "true"}, {"6375", "false"}, {"20608", "true"}, {"6364", "false"},},
	[32] = {{"945", "true"}, {"8512", "true"}, {"959", "true"}, {"8012", "true"}, {"6041", "false"}, {"421", "true"}, {"8499", "false"},},
	[34] = {{"6495", "false"}, {"10406", "false"}, {"8058", "false"},},
	[36] = {{"16339", "false"}, {"8010", "false"}, {"10495", "true"}, {"20610", "false"}, {"10412", "true"}, {"10585", "false"},},
	[38] = {{"10478", "false"}, {"8249", "false"}, {"8161", "true"}, {"10456", "false"}, {"8170", "false"}, {"6392", "false"}, {"10391", "false"},},
	[40] = {{"8134", "true"}, {"8235", "true"}, {"8005", "true"}, {"6377", "false"}, {"1064", "true"}, {"10447", "false"}, {"930", "false"}, {"6365", "false"}},
	[42] = {{"10537", "false"}, {"11314", "false"},},
	[44] = {{"10600", "false"}, {"10407", "false"}, {"10466", "false"}, {"10392", "false"},},
	[46] = {{"16341", "false"}, {"10496", "false"}, {"10622", "false"}, {"10472", "false"}, {"10586", "false"}, },
	[48] = {{"10431", "true"}, {"16355", "false"}, {"10526", "false"}, {"10395", "true"}, {"20776", "false"}, {"10413", "true"}, {"10427", "false"}, {"2860", "false"},},
	[50] = {{"10486", "true"}, {"10462", "false"}, {"15207", "false"}, {"10437", "false"},},
	[52] = {{"10442", "false"}, {"10467", "false"}, {"10448", "false"}, {"11315", "false"},},
	[54] = {{"10479", "false"}, {"10408", "false"}, {"10623", "false"}, },
	[56] = {{"10432", "true"}, {"16342", "false"}, {"10396", "true"}, {"10497", "false"}, {"15208", "false"}, {"10605", "false"}, {"10587", "false"},},
	[58] = {{"10538", "false"}, {"16356", "false"}, {"16387", "false"}, {"10428", "false"}, {"10473", "false"},},
}

LVLUP_SHAMAN_RESTORATION_ID_SPELL_INFO = { -- TODO
	[2] = {{"8071", "false"}, {"8042", "true"},},
	[6] = {{"332", "false"}, {"2484", "false"},},
	[8] = {{"324", "true"}, {"8018", "true"}, {"5730", "false"}, {"8044", "true"}, {"529", "true"},},
	[10] = {{"8024", "false"}, {"8075", "true"}, {"8050", "false"}, {"3599", "false"},},
	[12] = {{"547", "false"}, {"2008", "true"}, {"370", "true"}, {"1535", "false"},},
	[14] = {{"8154", "false"}, {"8045", "true"}, {"548", "true"},},
	[16] = {{"325", "true"}, {"8019", "true"},},
	[18] = {{"8027", "false"}, {"913", "false"}, {"8143", "false"}, {"6390", "false"}, {"8052", "false"},},
	[20] = {{"2645", "true"}, {"8033", "false"}, {"8004", "true"}, {"5394", "false"}, {"8056", "false"}, {"915", "true"}, {"6363", "false"},},
	[22] = {{"131", "false"}, {"8498", "false"},},
	[24] = {{"905", "true"}, {"10399", "true"}, {"8181", "false"}, {"8155", "false"}, {"8160", "true"}, {"939", "true"}, {"20609", "false"}, {"8046", "true"},},
	[26] = {{"6196", "false"}, {"8030", "false"}, {"5675", "false"}, {"943", "false"}, {"8190", "false"},},
	[28] = {{"8227", "false"}, {"8038", "false"}, {"8184", "false"}, {"546", "false"}, {"8008", "false"}, {"6391", "false"}, {"8053", "false"},},
	[30] = {{"8232", "true"}, {"8177", "false"}, {"10595", "false"}, {"556", "true"}, {"6375", "false"}, {"20608", "true"}, {"6364", "false"},},
	[32] = {{"945", "true"}, {"8512", "true"}, {"959", "true"}, {"8012", "true"}, {"6041", "false"}, {"421", "true"}, {"8499", "false"},},
	[34] = {{"6495", "false"}, {"10406", "false"}, {"8058", "false"},},
	[36] = {{"16339", "false"}, {"8010", "false"}, {"10495", "true"}, {"20610", "false"}, {"10412", "true"}, {"10585", "false"},},
	[38] = {{"10478", "false"}, {"8249", "false"}, {"8161", "true"}, {"10456", "false"}, {"8170", "false"}, {"6392", "false"}, {"10391", "false"},},
	[40] = {{"8134", "true"}, {"8235", "true"}, {"8005", "true"}, {"6377", "false"}, {"1064", "true"}, {"10447", "false"}, {"930", "false"}, {"6365", "false"}},
	[42] = {{"10537", "false"}, {"11314", "false"},},
	[44] = {{"10600", "false"}, {"10407", "false"}, {"10466", "false"}, {"10392", "false"},},
	[46] = {{"16341", "false"}, {"10496", "false"}, {"10622", "false"}, {"10472", "false"}, {"10586", "false"}, },
	[48] = {{"10431", "true"}, {"16355", "false"}, {"10526", "false"}, {"10395", "true"}, {"20776", "false"}, {"10413", "true"}, {"10427", "false"}, {"2860", "false"},},
	[50] = {{"10486", "true"}, {"10462", "false"}, {"15207", "false"}, {"10437", "false"},},
	[52] = {{"10442", "false"}, {"10467", "false"}, {"10448", "false"}, {"11315", "false"},},
	[54] = {{"10479", "false"}, {"10408", "false"}, {"10623", "false"}, },
	[56] = {{"10432", "true"}, {"16342", "false"}, {"10396", "true"}, {"10497", "false"}, {"15208", "false"}, {"10605", "false"}, {"10587", "false"},},
	[58] = {{"10538", "false"}, {"16356", "false"}, {"16387", "false"}, {"10428", "false"}, {"10473", "false"},},
}

LVLUP_WARRIOR_ARMS_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}
LVLUP_WARRIOR_FURY_ID_SPELL_INFO = {
}

LVLUP_WARRIOR_PROTECTION_ID_SPELL_INFO = {
}

LVLUP_DRUID_FERAL_ID_SPELL_INFO = {
	[4] = {{"774", "true"}, {"8921", "true"},},
	[6] = {{"467", "true"}, {"5177", "true"},},
	[8] = {{"5186", "false"}, {"339", "false"},},
	[10] = {{"5232", "true"}, {"1058", "true"}, {"8924", "true"}, {"18960", "true"},},
	[12] = {{"5229", "false"}, {"8936", "false"}, ,
	[14] = {{"5211", "true"}, {"5187", "false"}, {"782", "true"}, {"5178", "false"},},
	[16] = {{"779", "true"}, {"1430", "true"}, {"8925", "true"},},
	[18] = {{"6808", "true"}, {"8938", "false"}, {"770", "true"}, {"1062", "false"}, {"2637", "true"},},
	[20] = {{"1735", "false"}, {"5188", "false"}, {"6756", "true"}, {"20484", "true"}, {"2912", "false"}, },
	[22] = {{"5221", "false"}, {"2090", "false"}, {"5179", "false"}, {"8926", "false"}, {"2908", "false"}, },
	[24] = {{"780", "false"}, {"5217", "false"}, {"1822", "true"}, {"8939", "false"}, {"2782", "true"}, {"1075", "true"},},
	[26] = {{"6809", "false"}, {"1850", "true"}, {"5189", "false"}, {"2893", "false"}, {"8949", "false"},},
	[28] = {{"3029", "true"}, {"5209", "false"}, {"8998", "false"}, {"9492", "true"}, {"2091", "true"}, {"8927", "false"}, {"5195", "false"},},
	[30] = {{"6800", "true"}, {"6798", "false"}, {"783", "true"}, {"5234", "true"}, {"8940", "true"}, {"740", "false"}, {"20739", "true"}, {"5180", "false"},},
	[32] = {{"5225", "false"}, {"6785", "true"}, {"9490", "false"}, {"22568", "true"}, {"6778", "false"},},
	[34] = {{"1823", "true"}, {"8972", "false"}, {"769", "false"}, {"3627", "true"}, {"8914", "true"}, {"8950", "false"}, {"8928", "false"},},
	[36] = {{"6793", "false"}, {"9005", "true"}, {"9493", "true"}, {"22842", "false"}, {"8941", "true"},},
	[38] = {{"5201", "true"}, {"8992", "false"}, {"8903", "false"}, {"6780", "false"}, {"5196", "false"}, {"8955", "true"}, {"18657", "true"},},
	[40] = {{"20719", "true"}, {"9634", "true"}, {"22827", "true"}, {"8907", "true"}, {"8910", "true"}, {"8918", "false"}, {"20742", "true"}, {"8929", "false"}, {"16914", "false"}, {"29166", "true"},},
	[42] = {{"6787", "false"}, {"9745", "false"}, {"9747", "false"}, {"9750", "false"}, {"8951", "false"},},
	[44] = {{"1824", "true"}, {"9752", "true"}, {"9754", "false"}, {"9758", "false"}, {"9756", "true"}, {"22812", "true"},},
	[46] = {{"8983", "false"}, {"9821", "true"}, {"9823", "true"}, {"9829", "false"}, {"9839", "true"}, {"8905", "false"}, {"9833", "false"},},
	[48] = {{"9845", "false"}, {"9849", "true"}, {"22828", "true"}, {"9856", "true"}, {"9852", "true"},},
	[50] = {{"9866", "false"}, {"9880", "false"}, {"9888", "false"}, {"9884", "true"}, {"9862", "false"}, {"20747", "true"}, {"9875", "false"}, {"17401", "false"},},
	[52] = {{"9892", "false"}, {"9894", "false"}, {"9898", "false"}, {"9840", "false"}, {"9834", "false"},},
	[54] = {{"9830", "true"}, {"9904", "true"}, {"9908", "false"}, {"9857", "false"}, {"9910", "true"}, {"9912", "false"}, {"9901", "true"},},
	[56] = {{"9827", "true"}, {"9889", "false"}, {"22829", "true"},},
	[58] = {{"9850", "true"}, {"9867", "false"}, {"9881", "false"}, {"9841", "true"}, {"9876", "false"}, {"9835", "false"}, {"9853", "true"}, {"18658", "true"},},
}

LVLUP_DRUID_BALANCE_ID_SPELL_INFO = {
}

LVLUP_DRUID_RESTORATION_ID_SPELL_INFO = {
}

LVLUP_PRIEST_SHADOW_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_PRIEST_HOLY_ID_SPELL_INFO = {
}

LVLUP_PRIEST_DISCIPLINE_ID_SPELL_INFO = {
}

LVLUP_MAGE_FROST_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_MAGE_FIRE_ID_SPELL_INFO = {
}

LVLUP_MAGE_ARCANE_ID_SPELL_INFO = {
}

LVLUP_WARLOCK_DEMONOLOGY_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_WARLOCK_DESTRUCTION_ID_SPELL_INFO = {
}

LVLUP_WARLOCK_AFFLICTION_ID_SPELL_INFO = {
}

LVLUP_ROGUE_COMBAT_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_ROGUE_ASSASSINATION_ID_SPELL_INFO = {
}

LVLUP_ROGUE_SUBTETLY_ID_SPELL_INFO = {
}

LVLUP_PALADIN_RETRIBUTION_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_PALADIN_PROTECTION_ID_SPELL_INFO = {
}

LVLUP_PALADIN_HOLY_ID_SPELL_INFO = {
}

LVLUP_HUNTER_BEASTMASTER_ID_SPELL_INFO = {
	[2] = {{"XXXXXX", "false"}, },
	[4] = {{"XXXXXX", "false"}, },
	[6] = {{"XXXXXX", "false"}, },
	[8] = {{"XXXXXX", "false"}, },
	[10] = {{"XXXXXX", "false"}, },
	[12] = {{"XXXXXX", "false"}, },
	[14] = {{"XXXXXX", "false"}, },
	[16] = {{"XXXXXX", "false"}, },
	[18] = {{"XXXXXX", "false"}, },
	[20] = {{"XXXXXX", "false"}, },
	[22] = {{"XXXXXX", "false"}, },
	[24] = {{"XXXXXX", "false"}, },
	[26] = {{"XXXXXX", "false"}, },
	[28] = {{"XXXXXX", "false"}, },
	[30] = {{"XXXXXX", "false"}, },
	[32] = {{"XXXXXX", "false"}, },
	[34] = {{"XXXXXX", "false"}, },
	[36] = {{"XXXXXX", "false"}, },
	[38] = {{"XXXXXX", "false"}, },
	[40] = {{"XXXXXX", "false"}, },
	[42] = {{"XXXXXX", "false"}, },
	[44] = {{"XXXXXX", "false"}, },
	[46] = {{"XXXXXX", "false"}, },
	[48] = {{"XXXXXX", "false"}, },
	[50] = {{"XXXXXX", "false"}, },
	[52] = {{"XXXXXX", "false"}, },
	[54] = {{"XXXXXX", "false"}, },
	[56] = {{"XXXXXX", "false"}, },
	[58] = {{"XXXXXX", "false"}, },
}

LVLUP_HUNTER_MARKSMANSHIP_ID_SPELL_INFO = {
}

LVLUP_HUNTER_SURVIVAL_ID_SPELL_INFO = {
}