function LVLUP_LevelUpHunterSurvival(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_HUNTER_SURVIVAL_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpHunterBeastMaster(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_HUNTER_BEASTMASTER_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpHunterMarksmanship(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_HUNTER_MARKSMANSHIP_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end