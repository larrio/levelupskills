function LVLUP_LevelUpDruidFeral(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			print("Don't forget to get your bear form !")
		elseif (new_level == 14) then
			print("Don't forget to take your cure poison quest !")
		elseif (new_level == 16) then
			print("Don't forget to get your aquatic form !")
		elseif (new_level == 20) then
			print("Don't forget to get your cat form ! Also, don't forget to buy the right seed to use your new Rebirth spell")
		elseif (new_level == 30) then
			print("Don't forget to get your travel form ! Also, don't forget to buy the right seed to use your new Rebirth spell")
		elseif (new_level == 40) then
			print("Don't forget to get your Dire Bear form ! Also, don't forget to buy the right seed to use your new Rebirth spell")
		elseif (new_level == 50) then
			print("Don't forget to buy the right seed to use your new Rebirth spell")
		end

		WHP_ProcessLevelUp(LVLUP_DRUID_FERAL_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpDruidBalance(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_DRUID_BALANCE_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpDruidRestoration(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_DRUID_RESTORATION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end