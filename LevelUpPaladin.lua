function LVLUP_LevelUpPaladinRetribution(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_PALADIN_RETRIBUTION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpPaladinProtection(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_PALADIN_PROTECTION_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end

function LVLUP_LevelUpPaladinHoly(new_level)
	if (new_level ~= 60) then
		if (new_level == 10) then
			-- TODO : print if important level
		end

		WHP_ProcessLevelUp(LVLUP_PALADIN_HOLY_ID_SPELL_INFO, new_level)
	else
		LVLUP_CongratsMaxLevel()
	end
end